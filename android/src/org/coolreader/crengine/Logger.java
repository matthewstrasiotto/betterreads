package org.coolreader.crengine;

/**
 * @author Probably Vadim Lopatim
 * Documented by Matthew Strasiotto
 * Provides an interface for loging application run-time information,
 * mostly to abstract the process of naming threads.
 *
 * I assume method names are kept short to reduce the amount of time to type  logging statements
 * as it is such a common and trivial operation
 * I am documenting them to mitigate any mystification that may arise from this
 *
 * The implementation of this should allow the user to attach a name to their logger.
 */
public interface Logger
{
	/**
	 * Tags the log entry with INFO
	 * For providing extra context to the
	 * @param msg, the message to display
	 */
	public void i(String msg);

	/**
	 * As above, but allows Exception to also be printed
	 * @param msg
	 * @param e
	 */
	public void i(String msg, Exception e);

	/**
	 * Tags the log with WARN, indicating its a runtime warning
	 * @param msg
	 */
	public void w(String msg); 
	public void w(String msg, Exception e);

	/**
	 * For ERROR logging, tags with ERROR
	 * @param msg
	 */
	public void e(String msg); 
	public void e(String msg, Exception e);

	/**
	 * for DEBUG logging, tags with DEBUG
	 * @param msg
	 */
	public void d(String msg); 
	public void d(String msg, Exception e);

	/**
	 * For VERBOSE logging (saying everything) tags with VERBOSE
	 * @param msg
	 */
	public void v(String msg); 
	public void v(String msg, Exception e);
	public void setLevel( int level );

	/**
	 * @author Matthew Strasiotto
	 * For logging of fsm states
	 * @param msg, the current fsm state
	 */
	public void fsm(String msg);
}
