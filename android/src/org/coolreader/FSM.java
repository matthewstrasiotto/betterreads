package org.coolreader;

/**
 * @author Matthew Strasiotto
 * Namespace for finite state machine strings and names
 */
public interface FSM
{
    public static interface machines
    {
        public static final String bm = "BOOKMARKS";
    }

    public static final String root = "ROOT_MENU";
    public static final String reader = "READER_VIEW";
    public static final String bm = "BOOKMARK_DIALOG";
    public static final String goBm = "FOLLOW_BOOKMARK";
    public static final String addBm = "ADD_BOOKMARK";
}
