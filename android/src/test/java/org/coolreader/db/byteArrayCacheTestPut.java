package org.coolreader.db;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Matthew Strasiotto, mstr3336, SID: 440 376 194
 * Test Suite for the ByteArrayCache "put" method
 * A minimum of 6 tests are required for this method, 
 * given that cyclomatic analysis revealed that the method's has a
 * complexity of 6
 * 
 * 
 */
public class byteArrayCacheTestPut {
	private ByteArrayCache byteCache;
	private int maxSize;

	private final String [] exampleIds = ByteArrayCacheTestTemplate.exampleIds;
	private final byte[][] exampleData = ByteArrayCacheTestTemplate.exampleData;

	private final int expectedValidElementsSize = 6;

	/**
	 *
	 */
	@Before
	public void setUp() throws Exception 
	{
		maxSize = 10;
		byteCache = new ByteArrayCache(maxSize);
	}
	/*
	 *Helper method to add a small set of valid elements to the bytecache
	 * Should have a length of 6.
	 */
	private void addValidElements() 
	{
		for (int i = 0; i < exampleIds.length; i++)
			byteCache.put(exampleIds[i], exampleData[i]);
	}
	
	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception 
	{
		byteCache = null;
	}

	/*
	 * Test use of null id entry.
	 * Should throw exception
	 */
	@Test(expected = NullPointerException.class)
	public void testPutNullId() 
	{
		String id = null;
		byte [] data = new byte[] {0x00};

		byteCache.put(id, data);

		assertEquals(data, byteCache.get(id));

	}
	/*
	 * Tests path 2
	 */
	@Test
	public void testValidElementsGiveExpectedSize() 
	{
		addValidElements();
		assertEquals("currentSize should be equal to number of bytes in cache!\n",
				byteCache.getCurrentSize(),expectedValidElementsSize);
	}
	/*
	 * Tests Path 2
	 */
	@Test
	public void testValidElementsCanBeFound() 
	{
		addValidElements();
		for (int i = 0; i < exampleIds.length ; i++)
			assertEquals(byteCache.get(exampleIds[i]), exampleData[i]);
		
	}
	
	/*
	 * Test that the currentSize of the list is the same as it was if adding an empty item
	 * Tests branch 1
	 */
	@Test
	public void testPutEmptyItemWontChangeSize() 
	{
		int prevSize = byteCache.getCurrentSize();
		String id = "My Item!";
		byteCache.put(id, null);
		assertTrue("Size should be unchanged when adding empty item!\n",
				prevSize == byteCache.getCurrentSize());
	}
	
	/*
	 * Test that adding elements with colliding ids, and same datalengths
	 * Should leave currentSize unchanged
	 * Tests branch 6
	 */
	@Test
	public void testPutCollisonsSameDataWontChangeSize() 
	{
		addValidElements();
		int prevSize = byteCache.getCurrentSize();
		addValidElements();
		assertTrue("Adding elements with identical data but colliding ids shouldn't change size!\n",
				prevSize == byteCache.getCurrentSize());
	}
	
	/*
	 * Test that adding element with colliding id, and different datalength
	 * Should change currentSize
	 * Covers branches 5, 4
	 */
	@Test
	public void testPutCollisionsDifferentDataWillChangeSize() 
	{
		addValidElements();
		int firstSize = byteCache.getCurrentSize();
		int prevSize = firstSize;
		byte [] newData = null;
		byteCache.put(exampleIds[0], newData);
		
		assertFalse("Putting a colliding id with different size should change size!",
				prevSize == byteCache.getCurrentSize());
		
		prevSize = byteCache.getCurrentSize();
		addValidElements();
		assertTrue("Reassigning entry with more data should increase size!",
				prevSize < byteCache.getCurrentSize());
		assertTrue("Assigning data back to items should restore size!", 
				firstSize == byteCache.getCurrentSize());
	}
	
	/*
	 * Test branch 3
	 */
	@Test
	public void testNullOnNullCollision() 
	{
		String id = "NullID";
		byte [] data = null;
		int startSize = byteCache.getCurrentSize();
		int prevSize = startSize;
		byteCache.put(id, data);
		prevSize = byteCache.getCurrentSize();
		byteCache.put(id, data);
		
		assertTrue("Null on null collision should not change size!",
				prevSize == byteCache.getCurrentSize());
		assertEquals("Data set to null should be null!",
				null, byteCache.get(id));
	}
	
}
