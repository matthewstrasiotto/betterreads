package org.coolreader.db;

import junit.framework.Assert;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Bottom-up testing classes for the methods within ByteArrayCache,
 * METHODS NOT TESTED HERE:
 * find() is here not tested, as Jahvani Patel has implemented tests for this method, already
 * put() is here not tested, as I have already written a test class for this method
 *
 *
 * @author Matthew Strasiotto
 *
 */
public class ByteArrayCacheTest {

    //@Rule
    //public ExpectedException expected = ExpectedException.none();

    private final String [] exampleIds = ByteArrayCacheTestTemplate.exampleIds;
    private final byte [][] exampleData = ByteArrayCacheTestTemplate.exampleData;
    private ArrayList<ByteArrayCache.ByteArrayItem> testList;
    private ByteArrayCache byteCache;
    private int maxSize;
    private int startingSize;

    @Before
    public void setUp() throws Exception
    {
        maxSize = 10;
        byteCache = new ByteArrayCache(maxSize);
        testList = new ArrayList<ByteArrayCache.ByteArrayItem>();
        startingSize = 0;
        for (int i = 0; i < exampleIds.length && i < exampleData.length; i++)
        {
            byteCache.put(exampleIds[i], exampleData[i]);
            testList.add(i, new ByteArrayCache.ByteArrayItem(exampleIds[i], exampleData[i]));
            startingSize += exampleData[i].length;
        }

        startingSize = byteCache.getCurrentSize();
        //expected = ExpectedException.none();
    }

    @After
    public void tearDown() throws Exception
    {
        byteCache = null;
        testList = null;
        startingSize = 0;
        //expected = null;
    }

    /**
     * Should throw exception
     */
    @Test//(expected = NullPointerException.class)
    public void getNullId()
    {
     byte [] out = null;
     //expected.expect(NullPointerException.class);
     out = byteCache.get(null);

     assertEquals(null, out);
    }

    /**
     * Test trying to get Ids which should be mapped within the cache
     * Should respond corresponding data
     */
    @Test
    public void get()
    {
        byte [] out = null;
        for (int i = 0; i < exampleData.length && i < exampleIds.length ; i++)
        {
            assertEquals(exampleData[i], byteCache.get(exampleIds[i]));
        }
    }

    /**
     * Test trying to get() ids that aren't mapped to the cache, expecting null
     */
    @Test
    public void getWrongId()
    {
        assertEquals(null, byteCache.get("I JUST AM NOT THERE"));
    }


    private boolean listIsSameAsExample()
    {
        ArrayList<ByteArrayCache.ByteArrayItem> itemArrayList = byteCache.getList();
        if (itemArrayList.size() != testList.size()) return false;
        int i = 0;
        for (i = 0; i < testList.size(); i++)
        {
            if (itemArrayList.get(i).data != testList.get(i).data) return false;
        }

        return true;
    }

    @Test
    public void remove()
    {


        assertTrue(ByteArrayCacheTestTemplate.ExceptionStrings.contentsShouldStartSame, listIsSameAsExample());
        int oldSize = byteCache.getCurrentSize();
        String id = exampleIds[0];
        int lengthChange = exampleData[0].length;

        byteCache.remove(id);
        assertEquals(null, byteCache.get(id));
        assertNotEquals(ByteArrayCacheTestTemplate.ExceptionStrings.sizeChange, oldSize, byteCache.getCurrentSize());

        assertFalse(ByteArrayCacheTestTemplate.ExceptionStrings.contentsShouldChange, listIsSameAsExample());

        assertEquals(ByteArrayCacheTestTemplate.ExceptionStrings.sizeDiffCorrect,oldSize-lengthChange, byteCache.getCurrentSize());
    }


    /**
     * Should throw exception
     */
    @Test//(expected =  NullPointerException.class)
    public void removeNull()
    {
        //expected.expect(NullPointerException.class);
        byteCache.remove(null);

    }

    /**
     * Shouldn't change size, and shouldn't be able to find the object
     */
    @Test
    public void removeInvalid()
    {
        assertTrue(ByteArrayCacheTestTemplate.ExceptionStrings.contentsShouldStartSame, listIsSameAsExample());

        byteCache.remove("I AM NOT A VALID ID");

        assertTrue(ByteArrayCacheTestTemplate.ExceptionStrings.contentsDontChange, listIsSameAsExample());

    }

    @Test
    public void clear()
    {
        assertTrue(ByteArrayCacheTestTemplate.ExceptionStrings.contentsShouldStartSame, listIsSameAsExample());
        byteCache.clear();
        assertTrue("List should be empty!",byteCache.listIsEmpty());

        assertEquals("Size should be 0!",0, byteCache.getCurrentSize());
    }

    @Test
    public void getCurrentSize()
    {
        assertEquals(startingSize, byteCache.getCurrentSize());

        byte [] newData = {0x00, 0x01, 0x69};
        String newId = "I am a brand new id";
        byteCache.put(newId, newData);

        assertEquals(ByteArrayCacheTestTemplate.ExceptionStrings.sizeDiffCorrect, startingSize+newData.length, byteCache.getCurrentSize());

        byteCache.remove(newId);
        assertEquals(ByteArrayCacheTestTemplate.ExceptionStrings.sizeDiffCorrect, startingSize, byteCache.getCurrentSize());
    }

    /**
     * The following was implemented for testing purposes, but is a useful method
     *
     */
    @Test
    public void listIsEmpty()
    {
        byte [] newByte = {0x69};
        assertFalse(byteCache.listIsEmpty());

        for (String id : exampleIds)
        {
            byteCache.remove(id);
        }
        assertTrue(byteCache.listIsEmpty());
        byteCache.put("AN ID", newByte);
        assertFalse(byteCache.listIsEmpty());
        byteCache.clear();
        assertTrue(byteCache.listIsEmpty());
    }

    /**
     * The following was implemented for testing purposes, and should not see external use,
     * it has no public access modifier, but it does have protected.
     */
    @Test
    public void getList()
    {
        byte [] newByte = {0x69};
        assertTrue(listIsSameAsExample());
        byteCache.put("NEW ID", newByte);
        assertFalse(listIsSameAsExample());
    }
}