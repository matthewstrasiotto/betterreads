package org.coolreader.db;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Matthew Strasiotto
 * Test class for the methods within CoverDB that call upon
 * the bottom-level class, ByteArrayCache, which has been
 * comprehensively tested
 */
public class CoverDBTest {

    private CoverDB covers;
    private final byte [][] exampleData = ByteArrayCacheTestTemplate.exampleData;
    private final String [] exampleIds  = ByteArrayCacheTestTemplate.exampleIds;
    private final String expectedSetupBooksMsg = "Should be able to find cached books by their ids!";
    @Before
    public void setUp() throws Exception
    {
        covers = new CoverDB();
        //Put some data in the coverDB
        for (int i = 0; i < exampleData.length && i < exampleIds.length; i++)
        {
            covers.saveBookCoverpage(exampleIds[i], exampleData[i]);
        }

    }

    @After
    public void tearDown() throws Exception
    {
        covers = null;
    }

    /**
     * Helper method, check that the example books that should be loaded in
     * setUp are loaded, and that covers is correctly initialized
     * @return
     */
    private boolean checkExamples()
    {
        if (covers == null) return false;
        int i = 0;
        for (i = 0; i < exampleIds.length && i < exampleData.length; i++)
        {
            if (!Arrays.equals(exampleData[i] ,covers.loadBookCoverpage(exampleIds[i]))) return false;
        }
        return true;
    }

    /**
     * Test me, just wraps the ByteArrayCache clear() method for its
     * Member variable
     */
    @Test
    public void clearCaches()
    {
        int i = 0;
        //Check first you can find the books
        //Then check you can't after clearing
        assertTrue(expectedSetupBooksMsg, checkExamples());
        covers.clearCaches();
        for (i = 0; i < exampleIds.length && i <exampleData.length; i++)
        {
            assertEquals("Shouldn't be able to find books after clearCaches!",null, covers.loadBookCoverpage(exampleIds[i]));
        }

    }

    /**
     * Test me, seems to check that the page hasnt been saved
     * And if its not there, will save it
     */
    @Test
    public void saveBookCoverpage()
    {
        String savedBook = "NEW_BOOK";
        byte [] savedData = {0x69, 0x04, 0x20};
        //Check the two books from setup are there
        assertTrue(expectedSetupBooksMsg, checkExamples());
        //Now check a book isn't there
        assertEquals(null, covers.loadBookCoverpage(savedBook));
        //Now add that book and check it is there
        covers.saveBookCoverpage(savedBook, savedData);
        assertArrayEquals(expectedSetupBooksMsg, savedData, covers.loadBookCoverpage(savedBook));
    }

    /**
     * Returns the byte array of data associated with a book's id
     * Should return null if the book is not opened, or there is no such book
     */
    @Test
    public void loadBookCoverpage()
    {
        //Check the loading of just the exampleData/exampleIds
        assertTrue(expectedSetupBooksMsg, checkExamples());
        assertEquals(null, covers.loadBookCoverpage("NEW_BOOK"));
    }

    /**
     * Removes the book's coverpage from the cache
     */
    @Test
    public void deleteCoverpage()
    {
        int i = 0;
        String toDelete = exampleIds[i++];
        //Check the examples are present
        //Delete one example, not the other
        //Check the deleted example is not present, but the other is
        assertTrue(expectedSetupBooksMsg, checkExamples());
        covers.deleteCoverpage(toDelete);
        assertEquals(null, covers.loadBookCoverpage(toDelete));
        //Now check that the rest are there
        for (; i < exampleData.length && i < exampleIds.length;i++)
        {
            assertArrayEquals(exampleData[i], covers.loadBookCoverpage(exampleIds[i]));
        }
    }
}