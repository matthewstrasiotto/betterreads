package org.coolreader.db;

class ByteArrayCacheTestTemplate
{


    static final String [] exampleIds =
        {
                "Matt",
                "69420"
        };

    static final byte[][] exampleData =
            {
                    {0x00, 0x01, 0x02},
                    {0x69, 0x42, 0x00}
            };

    static class ExceptionStrings
    {
        public static final String nullPtr = "Null pointer given!";
        public static final String sizeChange = "Size should change with operation!";
        public static final String sizeDiffCorrect = "Change in size should reflect amount of data changed!";
        public static final String contentsShouldStartSame = "The contents of the test arraylist and the internal list should be the same!";
        public static final String contentsShouldChange = "Contents should change!";
        public static final String contentsDontChange = "Contents should remain uncahnged with invalid input!";
    }

}
