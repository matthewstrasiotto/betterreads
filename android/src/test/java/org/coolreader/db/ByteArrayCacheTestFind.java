package org.coolreader.db;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;

import java.util.ArrayList;

import org.coolreader.db.ByteArrayCache.ByteArrayItem;

/**
 * @author Jahnavi Dinesh Patel (SID: 440527040)
 * This file is used to test the find(String id) method from the
 * ByteArrayCache.java class.
 *
 *
 */

public class ByteArrayCacheTestFind {

    ByteArrayCache byteArrayCache;
    private int maxSize = 10;
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        byteArrayCache = new ByteArrayCache(maxSize);
    }

    public void addByteArrayItemObject() {

        ByteArrayItem byteArrayItem1 = new ByteArrayItem("", new byte[] {0x00, 0x01, 0x02});
        byteArrayCache.put(String.valueOf(byteArrayItem1.id), byteArrayItem1.data);

        ByteArrayItem byteArrayItem2 = new ByteArrayItem("Rose", new byte[] {0x69, 0x42, 0x00});
        byteArrayCache.put(String.valueOf(byteArrayItem2.id), byteArrayItem2.data);

        ByteArrayItem byteArrayItem3 = new ByteArrayItem("Robert", new byte[] {0x30, 0x02, 0x09});
        byteArrayCache.put(String.valueOf(byteArrayItem3.id), byteArrayItem3.data);

        ByteArrayItem byteArrayItem4 = new ByteArrayItem("12345", new byte[] {0x31, 0x23, 0x29});
        byteArrayCache.put(String.valueOf(byteArrayItem4.id), byteArrayItem4.data);
    }


    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Path testing: 1,2,6,7
     */
    @Test
    public void testFindIdInEmptyList() {

        //check to see if the ByteArrayItem arraylist is empty or not
        assertTrue("ByteArrayItem arraylist has 0 ids", byteArrayCache.listIsEmpty());
        assertEquals("Currently there are no ids in the ByteArrayItem arraylist. Thus, it is empty!", 0, byteArrayCache.getList().size());

        ByteArrayItem test = new ByteArrayItem("idForEmptyList", new byte[] {0x69, 0x42, 0x00});
        assertEquals("Finding an id in an empty ByteArrayItem arraylist should return -1", -1, byteArrayCache.find(test.id));

    }

    /**
     * Path testing: 1,2,3,4,7
     */
    @Test
    public void testFindIdThatExistsInTheList() {
        //input test data
        addByteArrayItemObject();

        //check to make sure that the ByteArrayItem arraylist isn't empty
        assertFalse("ByteArrayItem arraylist has ids thus, it isn't empty", byteArrayCache.listIsEmpty());
        assertEquals("ByteArrayItem arraylist has 4 ids", 4, byteArrayCache.getList().size());


        ByteArrayItem test = new ByteArrayItem("", new byte[] {0x00, 0x01, 0x02});
        assertEquals("Given id is in index 0 i.e. first id in the ByteArrayItem arraylist", 0, byteArrayCache.getList().toString().indexOf(test.id));
        assertEquals("Empty id (NOT null id) is one of the id the ByteArrayItem arraylist", 0, byteArrayCache.find(test.id));

    }

    /**
     * Path testing: 1,2,3,5,2
     */
    @Test
    public void testFindIdThatsDoesNotExistsInList() {
        //input test data
        addByteArrayItemObject();

        //check to make sure that the ByteArrayItem arraylist isn't empty
        assertFalse("ByteArrayItem arraylist has ids thus, it isn't empty", byteArrayCache.listIsEmpty());
        assertEquals("ByteArrayItem arraylist has 4 ids", 4, byteArrayCache.getList().size());

        ByteArrayItem test1 = new ByteArrayItem("Rose", new byte[] {0x69, 0x42, 0x00});
        ByteArrayItem test2 = new ByteArrayItem("Robert", new byte[] {0x30, 0x02, 0x09});
        ByteArrayItem test3 = new ByteArrayItem("12345", new byte[] {0x31, 0x23, 0x29});
        ByteArrayItem test4 = new ByteArrayItem("Mark", new byte[] {0x20, 0x10, 0x05});


        //the given id in index 1 of the ByteArrayItem arraylist therefore, the variable 'i' is incremented 1 time
        assertEquals("Rose is one of the id the ByteArrayItem arraylist", 1, byteArrayCache.find(test1.id));

        //the given id in index 2 of the ByteArrayItem arraylist therefore, the variable 'i' is incremented 2 times
        assertEquals("Robert is one of the id the ByteArrayItem arraylist", 2, byteArrayCache.find(test2.id));

        //the given id in index 3 of the ByteArrayItem arraylist therefore, the variable 'i' is incremented 3 times
//        assertNotEquals("Given id is in index 3 thus, its not the first element in ByteArrayItem arraylist", 1, byteArrayCache.getList().toString().indexOf(test3.id));
        assertEquals("12345 is one of the id the ByteArrayItem arraylist", 3, byteArrayCache.find(test3.id));

        //iterate through the whole list to find the given id
        assertEquals("Mark doesn't exist as one of the ids in the ByteArrayItem arraylist", -1, byteArrayCache.find(test4.id));

    }

}