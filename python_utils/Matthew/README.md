# FSM Log Parser
This document will describe the operation of this script, and also the meaning of the .csv files it will output

## Requirements 
Python 3.6 or later should be installed.
Within the user’s  python environment, the packages listed  within FSM_Log_Parser/requirements.txt should be installed. 

To ensure this,  from within FSM_Log_Parser, the user should run the following command:

`pip install -r requirements.txt`

This will ensure that all of the dependencies of the module will be resolved.


## Running the script
From a terminal within this folder, inputting the following command:

`python FSM_Log_Parser -h`

Will print the help document describing what arguments the module accepts.
The most important arguments are `--file_log` and `--clipboard_in`.
These specify where the log input to parse should be loaded from
To run against the example input logs input the following command:
`python FSM_Log_Parser -f sample/filtered.txt`

or:

`python FSM_Log_Parser -f sample/full.txt`

The output this produces will be stored in `./out/` , with subdirectories responding to FSM names.

For this example, (A log exercising the ‘Bookmarks’ FSM), the outputs may be found in ` .out/BOOKMARKS/ `

For a given machine, there will be two files, ` adjacency.csv ` , and ` probability.csv`
## Understanding the Output
The contents of  adjacency.csv will resemble the following:

This is an adaptation of an adjacency matrix, for which  each row corresponds to a specific FSM state, with a count of its observed transitions (generated from the log)  to other states displayed under those states corresponding columns.
From this example, it can be seen that the log observed two transitions from the ROOT_MENU->READER_VIEW,in the specific session.


The contents of  `probability.csv` will resemble the following:

This follows the same structure as `adjacency.csv` , however, for a given row, each entry indicates the observed probability of a transition to the corresponding column’s state.

In this example, it can be seen that a probability of a transition from READER_VIEW->BOOKMARK_DIALOG has been observed to be %83, while READER_VIEW->ROOT_MENU has an observed probability of ~17%
# Generating Log Files
At time of writing this, operation of the android application will only generate FSM log statements for the Bookmark FSM, though the parser can interpret any FSM log statements. 

As the developer implements features within the program, it is recommended that they include corresponding state logging statements, as per the feature they are including. 

An FSM logging method implemented to assist with this has been implemented, is called according to the  following syntax:

`L.fsm(String machine, String state);`

A java interface FSM.java has been added to the project, which serves as a namespace for machine names and their various states to avoid hardcoding of these statements.

Log statements for general application logging follow the format observable in `sample/full.txt`

Log statements for FSM logging follow the format observable in  `sample/filtered.txt`

This is just a filtered version of the `full.txt` file, with superfluous statements discarded.
