# -*- coding: utf-8 -*-
"""
@author Matthew Strasiotto

Logcat FSM parser
Assumes input resembles standard CoolReader3 logging output:
    FSM logs:
        05-19 19:26:01.623 5857-5857/org.coolreader V/cr3: G|fsm|BOOKMARKS|s|BOOKMARK_DIALOG
    General logs:
        05-19 19:24:56.305 5857-5857/org.coolreader D/cr3: G|en| mount entry: rootfs on / type rootfs (ro,seclabel,size=616056k,nr_inodes=154014)
"""
import pandas as pd
import numpy  as np
import sys, pathlib
import argparse

fsm_tag     = 'fsm'
tag_col     = 'tag'
machine_col = 'machine'
type_col    = 'type'
state_col   = 'state' 

trans_sum   = 'Sum of Transitions'

colnames = ['useless',tag_col,machine_col,type_col,state_col]

root_dir        = './'
out_dir         = 'out/'
adjacency_name  = 'adjacency.csv'
probability_name= 'probability.csv'

""""
Read from the clipboard, and turn it into a dataframe, just taking the lines
tagged as FSM
"""
def clipboard_to_dframe():
    #Read from clipboard
    frame_out = pd.read_clipboard(sep='|', header=None, names=colnames)
    return frame_out

def text_file_to_dframe(file_path):
    frame_out = pd.read_table(file_path, sep='|', header=None, names=colnames)
    return frame_out
"""
Filter out any useless data from the log parsed to a dataframe
This include timestamps, thread info, and all lines that aren't tagged as fsm
"""
def drop_useless_data(full_log):
    frame_out = full_log
    #drop the timestamps and junks, filter out any that don't have a good tag col
    frame_out = (frame_out
                 .drop(columns={'useless'})
                 .dropna(axis='index', subset=[tag_col]))
    #Now drop any without the fsm tag
    frame_out = frame_out[frame_out[tag_col].str.match(fsm_tag)]
    
    for col in [machine_col, type_col, state_col]:
        frame_out[col] = frame_out[col].astype('category')
    return frame_out

"""
Get the list of unique machine names
"""
def get_machine_list(state_log):
    out = state_log[machine_col].dropna().unique()
    return out


"""
Get the log for just the specific machine you want
"""
def get_machine_log(machine_name, state_log):
    machine_log = state_log[state_log[machine_col].str.match(machine_name)].reset_index()
    return machine_log

"""
Get a list of each unique state for your machine
"""  
def get_machine_states(machine_log):
    
    machine_states = machine_log[state_col].unique()
    
    return machine_states

"""
Make an adjacency matrix for the given FSM, 
count the amount of time each edge is traversed
"""
def machine_adjacency_matrix(machine_log):
    states = get_machine_states(machine_log)
    out = pd.DataFrame(index=states,columns=states,dtype=np.int64).fillna(0)
    
    seq = machine_log[state_col]
    for state, next_state in zip(seq, seq[1:]):        
        """
        don't log circular/self transitions
        anything that produces a unique (and relevant) output should have its
        own state
        """
        if (state != next_state):
            out.loc[state,next_state]+= 1
        
    return out
    

"""
Process an entire machine
"""
def process_machine(machine_name, state_log):
    return machine_adjacency_matrix(get_machine_log(machine_name, state_log))

"""
Process all machines, mapping them each to a matrix, and each matrix to a
dict key
"""
def map_all_matrices(state_log):
    machines = get_machine_list(state_log)
    machine_dict = {}
    probability_dict = {}
    for machine in machines:
        machine_dict[machine] = process_machine(machine, state_log)
    return machine_dict

"""
Collect stats on all machines
Could discard along the diagonal (that is, where column labels == index labels)
Sum all cols in each row, and then get col/sum
Should produce a total of 1 across any given row
"""
def transition_prob_matrix(machine_matrix):
    machine_matrix = machine_matrix.copy()
    state_transitions = machine_matrix.columns
    
    
    machine_matrix.columns = machine_matrix.columns.add_categories([trans_sum])
    #for each row, sum all transitions and put in its own col
    machine_matrix[trans_sum] = machine_matrix.sum(axis=1)
    
    #now cast the matrix to float, because you want fractions
    machine_matrix = machine_matrix.astype(float)
    
    #For each state divide each of its transitions by its corresponding total
    #number of transitions, to get a probability
    for transition in state_transitions:
        machine_matrix.loc[:,transition] = machine_matrix[transition]/machine_matrix[trans_sum]
    
    #drop that total now
    machine_matrix = machine_matrix[state_transitions]
    return machine_matrix

def map_matrices_to_probs(machine_dict):
    prob_dict = {}
    #iterate through each machine matrix, and map it to a probability matrix
    for machine in machine_dict:
        prob_dict[machine] = transition_prob_matrix(machine_dict[machine])
    
    return prob_dict

"""
Top level script, do all the things!
"""
def main(argv):
    parser = argparse.ArgumentParser(description='Utility for parsing logcat output to FSM adjacency matrices,\nand determining transition probabilities for each state')
    
    io_args = parser.add_mutually_exclusive_group(required=True)
    
    io_args.add_argument("-f","--file_log", help="Specify the file from which to load the log", default=None)
    io_args.add_argument("-c", "--clipboard_in", help="Specify if input should be loaded from clipboard")
    
    parser.add_argument("-v","--verbosity",type=int, choices=[0,1,2],help="Specify how much info to print", default=0)
    args = parser.parse_args()
    
    
    root_used = root_dir
    out_used  = root_dir + out_dir
    #if the user specified a file path use that, otherwise copy from the clipboard
    state_log = text_file_to_dframe(args.file_log) if (args.file_log != None) else clipboard_to_dframe()
    
    state_log = drop_useless_data(state_log)
    #now, map all the matrices
    machine_adjacency_dict = map_all_matrices(state_log)
    #now, map all of the probabilities
    machine_probability_dict = map_matrices_to_probs(machine_adjacency_dict)
    
    if (args.verbosity >= 1):
        p = pathlib.Path('.')
        print("Directory Contents")
        for x in p.iterdir():
            print("\t",x)
        print()

    for machine in machine_adjacency_dict:
        machine_dir = out_used + machine + '/'
        pathlib.Path(machine_dir).mkdir(parents=True,exist_ok=True)
        if (args.verbosity >= 2):
            print(machine + ' adjacency')
            print(machine_adjacency_dict[machine])
            print()
            print(machine + ' probability')
            print(machine_probability_dict[machine])
            print()
        machine_adjacency_dict[machine].to_csv(machine_dir+adjacency_name)
        
        machine_probability_dict[machine].to_csv(machine_dir+probability_name)
    
    return

if __name__ == "__main__":
    main(sys.argv)